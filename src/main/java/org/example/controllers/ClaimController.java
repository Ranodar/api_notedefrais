package org.example.controllers;

import org.example.dtos.response.ClaimDto;
import org.example.services.impl.ClaimServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = {"http://localhost:8084"}, methods = {RequestMethod.GET, RequestMethod.POST})
@RestController
@RequestMapping("/api/claim")
public class ClaimController {

    @Autowired
    private ClaimServiceImpl _claimService;

    @GetMapping("")
    public ResponseEntity<List<ClaimDto>> getAllClaims() {
        return ResponseEntity.ok(_claimService.readClaims());
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<List<ClaimDto>> getClaimsByUser(@PathVariable int id) {
        return ResponseEntity.ok(_claimService.getClaimByIdUser(id));
    }

    @GetMapping("/{id}")
    public ResponseEntity getClaim(@PathVariable int id){

        try {
            return  ResponseEntity.ok(_claimService.readClaim(id));
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("claim_not_found");
        }

    }

    @PostMapping("")
    public ResponseEntity createClaim(@RequestBody ClaimDto claimDto) throws Exception {
        ClaimDto claimDtoSave = _claimService.createClaim(claimDto);
        try {
            return ResponseEntity.ok(claimDtoSave);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("create_claim_fail");
        }
    }

    @PostMapping("/{id}")
    public ResponseEntity updateClaim(@PathVariable String id, @RequestParam String date, String category, String company, double price) throws Exception {
        ClaimDto claimDto = _claimService.updateClaim(Integer.parseInt((id)), date, category, company, price);
        try {
            return new ResponseEntity<>(claimDto,HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("update_claim_fail");
        }
    }

    @PostMapping("/status/{id}")
    public ResponseEntity updateStatusClaim(@PathVariable String id) throws Exception {
        ClaimDto claimDto = _claimService.updateStatusClaim(Integer.parseInt((id)));
        try {
            return new ResponseEntity<>(claimDto,HttpStatus.OK);
        }catch (Exception e){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("update_status_fail");
        }
    }


}
