package org.example.repository;

import org.example.entity.Claim;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClaimRepository extends CrudRepository<Claim, Integer> {

    public List<Claim> findClaimsByIdUser(int idUser);

//    public Claim findById(long id);


}
