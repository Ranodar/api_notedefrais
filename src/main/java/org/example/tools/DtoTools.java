package org.example.tools;

import org.example.dtos.response.ClaimDto;
import org.example.entity.Claim;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class DtoTools {

    public ClaimDto convertToDto(Claim claim, ClaimDto claimDto){
        return new ModelMapper().map(claim, claimDto.getClass());
    }

    public Claim convertToEntity(Claim claim, ClaimDto claimDto){
        return new ModelMapper().map(claimDto, claim.getClass());
    }
}
